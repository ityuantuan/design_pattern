package com.allsofts.pattern.homework.factory.simple;

/**
 * @ProjectName: pattern
 * @Package: com.allsofts.pattern.homework.factory.simple
 * @description:
 * @Author: liyc
 * @CreateDate: 2018/8/24 下午9:08
 * @Version: 1.0
 * <p>Copyright: Copyright (c) 2018</p>
 */
public class Test {
    public static void main(String[] args) {
        FuShiKangFactory factory = new FuShiKangFactory();
        MiPhone phone = (MiPhone) factory.getPhone(MiPhone.class);
        System.out.println(phone.toString());
    }
}
