package com.allsofts.pattern.homework.factory.abs;

/**
 * @ProjectName: pattern
 * @Package: com.allsofts.pattern.homework.factory
 * @description:
 * @Author: liyc
 * @CreateDate: 2018/8/24 下午8:54
 * @Version: 1.0
 * <p>Copyright: Copyright (c) 2018</p>
 */
public class FuShiKangFactory extends AbstractFactory {


    @Override
    public Phone getInstance(Class<?> clazz) {
        try {
            return (Phone) clazz.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
