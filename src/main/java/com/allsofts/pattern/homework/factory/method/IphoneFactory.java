package com.allsofts.pattern.homework.factory.method;

/**
 * @ProjectName: pattern
 * @Package: com.allsofts.pattern.homework.factory.method
 * @description:
 * @Author: liyc
 * @CreateDate: 2018/8/24 下午9:26
 * @Version: 1.0
 * <p>Copyright: Copyright (c) 2018</p>
 */
public class IphoneFactory extends MethodAbsFactory {
    @Override
    public Phone getInstance() {
        return new Iphone();
    }
}
