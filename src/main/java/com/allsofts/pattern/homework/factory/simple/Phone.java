package com.allsofts.pattern.homework.factory.simple;

import lombok.*;
import lombok.experimental.Accessors;

/**
 * @ProjectName: pattern
 * @Package: com.allsofts.pattern.homework.factory.simple
 * @description:
 * @Author: liyc
 * @CreateDate: 2018/8/24 下午9:05
 * @Version: 1.0
 * <p>Copyright: Copyright (c) 2018</p>
 */
@Accessors(chain = true)
public class Phone {
    private String name;
    private String version;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }


}
