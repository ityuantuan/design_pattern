package com.allsofts.pattern.homework.factory.abs;

/**
 * @ProjectName: pattern
 * @Package: com.allsofts.pattern.homework.factory
 * @description:
 * @Author: liyc
 * @CreateDate: 2018/8/24 下午8:59
 * @Version: 1.0
 * <p>Copyright: Copyright (c) 2018</p>
 */
public class FactoryTest {
    public static void main(String[] args) {
        AbstractFactory abstractFactory = new FuShiKangFactory();
        Mphone camera = (Mphone) abstractFactory.getInstance(Mphone.class);
        System.out.println(camera);
    }
}
