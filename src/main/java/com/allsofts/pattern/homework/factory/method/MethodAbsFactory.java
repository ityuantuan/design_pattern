package com.allsofts.pattern.homework.factory.method;

/**
 * @ProjectName: pattern
 * @Package: com.allsofts.pattern.homework.factory.method
 * @description:
 * @Author: liyc
 * @CreateDate: 2018/8/24 下午9:24
 * @Version: 1.0
 * <p>Copyright: Copyright (c) 2018</p>
 */
public abstract class MethodAbsFactory {
    public abstract Object getInstance();
}
