package com.allsofts.pattern.homework.factory.abs;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * @ProjectName: pattern
 * @Package: com.allsofts.pattern.homework.factory
 * @description:
 * @Author: liyc
 * @CreateDate: 2018/8/24 下午8:57
 * @Version: 1.0
 * <p>Copyright: Copyright (c) 2018</p>
 */
@Data
@EqualsAndHashCode
@ToString
public class Mphone extends Phone{

    private String name;
    private String version;
}
