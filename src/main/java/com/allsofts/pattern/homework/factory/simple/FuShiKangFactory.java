package com.allsofts.pattern.homework.factory.simple;

/**
 * @ProjectName: pattern
 * @Package: com.allsofts.pattern.homework.factory.simple
 * @description:
 * @Author: liyc
 * @CreateDate: 2018/8/24 下午9:05
 * @Version: 1.0
 * <p>Copyright: Copyright (c) 2018</p>
 */
public class FuShiKangFactory {
    public Phone getPhone(Class<?> clazz){
        if(clazz.getName().equals(MiPhone.class.getName())){
            Phone phone = new MiPhone();

            ((MiPhone) phone).setLogo("xiaomi");
            phone.setName("xiaomi");
            phone.setVersion("version");
            return phone;
        }
        return null;

    }
}
