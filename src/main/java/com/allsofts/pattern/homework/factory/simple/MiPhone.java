package com.allsofts.pattern.homework.factory.simple;

import lombok.*;
import lombok.experimental.Accessors;

/**
 * @ProjectName: pattern
 * @Package: com.allsofts.pattern.homework.factory.simple
 * @description:
 * @Author: liyc
 * @CreateDate: 2018/8/24 下午9:06
 * @Version: 1.0
 * <p>Copyright: Copyright (c) 2018</p>
 */

public class MiPhone extends Phone {

    private String logo;

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    @Override
    public String toString() {
        return "MiPhone{" +
                "logo='" + logo + '\'' +
                "name='" + this.getName() + '\'' +
                "version='" + this.getVersion() + '\'' +
                '}';
    }
}
