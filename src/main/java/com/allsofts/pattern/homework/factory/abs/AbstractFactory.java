package com.allsofts.pattern.homework.factory.abs;

/**
 * @ProjectName: pattern
 * @Package: com.allsofts.pattern.homework.factory
 * @description:
 * @Author: liyc
 * @CreateDate: 2018/8/24 下午8:50
 * @Version: 1.0
 * <p>Copyright: Copyright (c) 2018</p>
 */
public abstract class AbstractFactory {
    public abstract Object getInstance(Class<?> clazz);
}
