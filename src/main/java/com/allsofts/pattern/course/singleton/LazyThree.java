package com.allsofts.pattern.course.singleton;

/**
 * 懒加载的单例
 * **/
public class LazyThree {

    private static boolean initialized = false;

    //默认使用lazyThree的时候，会先初始化内部类，如果没使用的话，内部类不加载
    private LazyThree(){
        synchronized (LazyThree.class){
            if (!initialized){
                initialized = !initialized;
            }else {
                throw new RuntimeException("单例被侵犯");
            }
        }
    }

    public static final LazyThree getInstance(){
        return LazyHolder.lazy;
    }

    private static class LazyHolder{
        private static final LazyThree lazy = new LazyThree();
    }
}
