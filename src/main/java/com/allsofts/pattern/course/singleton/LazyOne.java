package com.allsofts.pattern.course.singleton;

/**
 * 懒汉式单例，线程安全的单例
 **/
public class LazyOne {
    private static LazyOne instance = null;

    private LazyOne() {

    }

    /**
     * 使用synchronized方法
     **/
    public static synchronized LazyOne getInstanceOne() {
        if (null == instance) {
            instance = new LazyOne();
        }
        return instance;
    }


    /**
     * 使用双重验证锁
     **/
    public static LazyOne getInstanceTwo() {
        if (null == instance) {
            synchronized (LazyOne.class) {
                if (null == instance) {
                    instance = new LazyOne();
                }
                return instance;
            }
        }
        return instance;
    }
}
