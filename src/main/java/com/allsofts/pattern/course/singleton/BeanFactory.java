package com.allsofts.pattern.course.singleton;

import com.allsofts.pattern.course.singleton.register.RegisterMap;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class BeanFactory {

    private BeanFactory() {

    }

    private static Map<String, Object> ioc = new ConcurrentHashMap<>();

    public static Object getInstance(String className) {

        if (ioc.containsKey(className)){
            return ioc.get(className);
        }else {
            try {
                Object obj = Class.forName(className).newInstance();
                return ioc.put(className,obj);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }
}
