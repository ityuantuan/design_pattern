package com.allsofts.pattern.course.singleton.seriable;

import java.io.Serializable;

/**
 * 序列化单例，重写readResolve
 * **/
public class Seriable implements Serializable {
    public final static Seriable INSTANCE = new Seriable();

    private Seriable(){

    }
    public static Seriable getInstance(){

        return INSTANCE;
    }


    public Object readResolve(){
        return INSTANCE;
    }
}
