package com.allsofts.pattern.course.singleton;

import java.util.Random;
import java.util.Vector;

/**
 * 多例,vector线程安全的
 **/
public class ManySingleton {
    private static final Vector<ManySingleton> singletons = new Vector<>();

    static {
        singletons.add(new ManySingleton());
        singletons.add(new ManySingleton());
        singletons.add(new ManySingleton());
        singletons.add(new ManySingleton());
    }

    public static ManySingleton getInstance() {
        Random random = new Random();
        int ran = random.nextInt(singletons.size());
        return singletons.get(ran);
    }
}
