package com.allsofts.pattern.course.singleton.register;


import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


/**
 * 注册登记式，同springIOC
 * **/
public class RegisterMap {

    private RegisterMap() {

    }

    private static Map<String, Object> register = new ConcurrentHashMap<>();

    public static RegisterMap getInstance(String name) {

        if (null == name) {
            name = RegisterMap.class.getName();
        }
        if (register.get(name) == null) {
            register.put(name, new RegisterMap());
        }
        return (RegisterMap) register.get(name);
    }
}
