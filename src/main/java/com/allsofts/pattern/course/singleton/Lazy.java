package com.allsofts.pattern.course.singleton;

/**
 * 懒汉式单例，线程不安全的单例
 * **/
public class Lazy {
    private static Lazy instance = null;

    private Lazy(){

    }
    public static Lazy getInstance(){
        if (null == instance){
            instance = new Lazy();
        }
        return instance;
    }
}
