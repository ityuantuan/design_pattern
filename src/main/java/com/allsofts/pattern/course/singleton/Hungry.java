package com.allsofts.pattern.course.singleton;

/**
 * 饿汉式单例
 **/
public class Hungry {
    private static final Hungry instance = new Hungry();

    private Hungry() {
    }
    public static Hungry getInstance(){
        return instance;
    }
}
