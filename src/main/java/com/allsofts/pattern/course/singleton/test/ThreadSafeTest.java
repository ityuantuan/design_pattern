package com.allsofts.pattern.course.singleton.test;

import com.allsofts.pattern.course.singleton.LazyThree;
import com.allsofts.pattern.course.singleton.register.RegisterEnum;
import com.allsofts.pattern.course.singleton.seriable.Seriable;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.concurrent.CountDownLatch;

public class ThreadSafeTest {
    public static void main(String[] args) {
        long start = System.currentTimeMillis();
        int count = 100;
        CountDownLatch latch = new CountDownLatch(count);
        for (int i = 0; i < count; i++) {
            new Thread() {
                @Override
                public void run() {
                    try {
                        latch.await();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
//                    System.out.println(Hungry.getInstance());
//                    System.out.println(Lazy.getInstance());
//                    System.out.println(LazyOne.getInstanceOne());
                    System.out.println(LazyThree.getInstance());
                }
            }.start();
            latch.countDown();
        }

//        System.out.println(LazyThree.getInstance());
//        reflect();

//        seriable();
        long end = System.currentTimeMillis() - start;
        System.out.println("总耗时：" + end);

    }

    public static void reflect() {
        Class<?> clazz = LazyThree.class;
        Constructor[] cs = clazz.getDeclaredConstructors();
        for (Constructor c : cs) {
            c.setAccessible(true);
            try {
                Object o = c.newInstance();
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
        }
    }

    public static void enumTest(){
        RegisterEnum.INSTANCE.getInstance();
    }


    public static void seriable(){
        Seriable s1 = null;
        Seriable s2 = Seriable.getInstance();

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream("seriable.obj");
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(s2);
            oos.flush();
            oos.close();


            FileInputStream fis = new FileInputStream("seriable.obj");
            ObjectInputStream ois = new ObjectInputStream(fis);
            s1 = (Seriable) ois.readObject();
            ois.close();
            System.out.println(s1 == s2);

        }catch (Exception e){
            e.printStackTrace();
        }

    }

    /**
     * 发令枪，await线程阻塞，直到countDownLatch减到0，一起执行
     * **/
    public static void countDownLatch(){
        CountDownLatch countDownLatch = new CountDownLatch(100);
        for (int i = 100; i < 100; i++){
            int finalI = i;
            new Thread(){
                @Override
                public void run() {
                    try {
                        countDownLatch.await();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println(finalI);
                }
            }.start();
            countDownLatch.countDown();
        }
    }
}
