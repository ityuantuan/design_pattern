package com.allsofts.pattern.course.decorator.test;

import java.io.DataInputStream;
import java.io.FilterInputStream;
import java.io.InputStream;

/**
 * @ProjectName: pattern
 * @Package: com.allsofts.pattern.course.decorator.test
 * @description:
 * @Author: liyc
 * @CreateDate: 2018/8/22 下午9:52
 * @Version: 1.0
 * <p>Copyright: Copyright (c) 2018</p>
 */
public class DecoratorTest {
    public static void main(String[] args) {

        //为了某个实现类在不修改原始类的基础上进行动态的覆盖或者增加方法
        //该实现保持跟原有类的层级关系
        //采用装饰模式
        //装饰器模式实际上是一种非常特殊的适配器模式

        //虽然dataInputstream 功能更强大
        //DataInputstream 同样实现inputstream

        InputStream in = null;
        FilterInputStream fis = new DataInputStream(in);

    }
}
