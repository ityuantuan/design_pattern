package com.allsofts.pattern.course.decorator.passport.old;

import lombok.Data;

/**
 * @ProjectName: pattern
 * @Package: com.allsofts.pattern.course.adapter
 * @description:
 * @Author: liyc
 * @CreateDate: 2018/8/21 上午8:01
 * @Version: 1.0
 * <p>Copyright: Copyright (c) 2018</p>
 */
@Data
public class Memeber {

    private String username;
    private String password;

}
