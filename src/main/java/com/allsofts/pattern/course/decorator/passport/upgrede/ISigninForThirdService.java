package com.allsofts.pattern.course.decorator.passport.upgrede;

import com.allsofts.pattern.course.decorator.passport.old.ISigninService;
import com.allsofts.pattern.course.decorator.passport.old.ResultMsg;

/**
 * @ProjectName: pattern
 * @Package: com.allsofts.pattern.course.decorator.passport.upgrede
 * @description:
 * @Author: liyc
 * @CreateDate: 2018/8/22 下午10:03
 * @Version: 1.0
 * <p>Copyright: Copyright (c) 2018</p>
 */
public interface ISigninForThirdService extends ISigninService {
    ResultMsg loginForQQ(String openid);

    ResultMsg loginForWeChat(String openId);
    ResultMsg loginForToken(String token);
    ResultMsg loginForTel(String tel);

    ResultMsg loginForRegist(String userName,String password);
}
