package com.allsofts.pattern.course.decorator.passport;

import com.allsofts.pattern.course.decorator.passport.old.SigninService;
import com.allsofts.pattern.course.decorator.passport.upgrede.ISigninForThirdService;
import com.allsofts.pattern.course.decorator.passport.upgrede.SigninForThirdService;

/**
 * @ProjectName: pattern
 * @Package: com.allsofts.pattern.course.decorator.passport
 * @description:
 * @Author: liyc
 * @CreateDate: 2018/8/22 下午10:09
 * @Version: 1.0
 * <p>Copyright: Copyright (c) 2018</p>
 */
public class SigninTest {
    public static void main(String[] args) {
        //原来的功能依旧对外开放，依旧保留
        //新的功能同样的也可以使用
        ISigninForThirdService signinForThirdService = new SigninForThirdService(new SigninService());
        signinForThirdService.loginForQQ("sdfasdfa");

        /*
         * Decorator
         * Wrapper
         * 装饰器模式                                     适配器模式
         * =========================================================================
         * 是一种特别的适配器模式                           可以不保留层级关系
         * =========================================================================
         * 装饰者和被装饰者都要实现同一个接口                 适配者和被适配者没有必然的层级关系
         * 主要目的是为了扩展，依旧保留oop的关系              通常采用代理或者继承的形式进行包装
         * =========================================================================
         * 满足is-a的关系                                 满足has-a的关系
         * =========================================================================
         * 注重的是覆盖和扩展                              注重兼容和转换
         * =========================================================================
         * */
    }
}
