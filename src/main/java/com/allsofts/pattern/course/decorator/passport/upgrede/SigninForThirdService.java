package com.allsofts.pattern.course.decorator.passport.upgrede;


import com.allsofts.pattern.course.decorator.passport.old.ISigninService;
import com.allsofts.pattern.course.decorator.passport.old.ResultMsg;

/**
 * @ProjectName: pattern
 * @Package: com.allsofts.pattern.course.decorator.passport.upgrede
 * @description:
 * @Author: liyc
 * @CreateDate: 2018/8/22 下午10:05
 * @Version: 1.0
 * <p>Copyright: Copyright (c) 2018</p>
 */
public class SigninForThirdService implements ISigninForThirdService {

    private ISigninService signinService;

    public SigninForThirdService(ISigninService signinService) {
        this.signinService = signinService;
    }


    public ResultMsg loginForQQ(String openid) {
        return this.loginForRegist(openid, null);

    }

    public ResultMsg loginForWeChat(String openId) {
        return null;
    }

    public ResultMsg loginForToken(String token) {
        return null;
    }

    public ResultMsg loginForTel(String tel) {
        return null;
    }

    public ResultMsg loginForRegist(String userName, String password) {
        //1.认为openid全局唯一，可以当做用户名
        //2.密码默认为null
        //3.注册（在原有的系统创建一个用户）
        ResultMsg resultMsg = this.regist(userName, password);
        //4.调用原来的登录方法
        resultMsg = this.login(userName, password);

        return resultMsg;
    }

    @Override
    public ResultMsg regist(String username, String password) {
        return signinService.regist(username, password);
    }

    @Override
    public ResultMsg login(String username, String password) {
        return signinService.login(username, password);
    }
}
