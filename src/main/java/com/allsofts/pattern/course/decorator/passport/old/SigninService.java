package com.allsofts.pattern.course.decorator.passport.old;


/**
 * @ProjectName: pattern
 * @Package: com.allsofts.pattern.course.decorator.passport.old
 * @description:
 * @Author: liyc
 * @CreateDate: 2018/8/22 下午10:01
 * @Version: 1.0
 * <p>Copyright: Copyright (c) 2018</p>
 */
@Deprecated
public class SigninService implements ISigninService {
    /**
     * @param username
     * @param password
     * @return
     */
    public ResultMsg regist(String username, String password) {

        return new ResultMsg(200, "注册成功", new Memeber());
    }

    public ResultMsg login(String username, String password) {
        return new ResultMsg(200, "登录成功", new Memeber());
    }

}
