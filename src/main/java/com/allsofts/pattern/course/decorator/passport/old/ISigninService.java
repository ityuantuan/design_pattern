package com.allsofts.pattern.course.decorator.passport.old;

/**
 * @ProjectName: pattern
 * @Package: com.allsofts.pattern.course.decorator.passport.old
 * @description:
 * @Author: liyc
 * @CreateDate: 2018/8/22 下午9:59
 * @Version: 1.0
 * <p>Copyright: Copyright (c) 2018</p>
 */
public interface ISigninService {

    /**
     * @param username
     * @param password
     * @return
     */
    ResultMsg regist(String username, String password);

    ResultMsg login(String username, String password);
}
