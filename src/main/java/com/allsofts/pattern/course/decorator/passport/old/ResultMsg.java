package com.allsofts.pattern.course.decorator.passport.old;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @ProjectName: pattern
 * @Package: com.allsofts.pattern.course.adapter
 * @description:
 * @Author: liyc
 * @CreateDate: 2018/8/21 上午7:36
 * @Version: 1.0
 * <p>Copyright: Copyright (c) 2018</p>
 */
@Data
@AllArgsConstructor
public class ResultMsg {
    private int code;
    private String msg;
    private Object data;

}
