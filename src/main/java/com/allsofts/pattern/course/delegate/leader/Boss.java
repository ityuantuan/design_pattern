package com.allsofts.pattern.course.delegate.leader;

/**
 * @ProjectName: pattern
 * @Package: com.allsofts.pattern.course.delegate.leader
 * @description:
 * @Author: liyc
 * @CreateDate: 2018/8/10 上午12:18
 * @Version: 1.0
 * <p>Copyright: Copyright (c) 2018</p>
 */
public class Boss {
    //客户请求（boss），委派者（Leader），被委派者（target）
    //委派者要持有被委派者的引用
    //代理模式注重的是过程，委派模式注重的是结果
    //策略模式注重的是可扩展（外部可扩展），委派模式注重的是内部的灵活复用
    //委派模式就是静态代理和策略模式的一种组合
    public static void main(String[] args) {
        new Leader().doing("TargetB");
    }
}
