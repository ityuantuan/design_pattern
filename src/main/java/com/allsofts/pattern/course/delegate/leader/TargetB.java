package com.allsofts.pattern.course.delegate.leader;

/**
 * @ProjectName: pattern
 * @Package: com.allsofts.pattern.course.delegate.leader
 * @description:
 * @Author: liyc
 * @CreateDate: 2018/8/10 上午12:18
 * @Version: 1.0
 * <p>Copyright: Copyright (c) 2018</p>
 */
public class TargetB implements Itarget{
    @Override
    public void doing(String command) {
        System.out.println("targetB");
    }
}
