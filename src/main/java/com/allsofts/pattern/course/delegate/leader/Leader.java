package com.allsofts.pattern.course.delegate.leader;

import java.util.HashMap;
import java.util.Map;

/**
 * @ProjectName: pattern
 * @Package: com.allsofts.pattern.course.delegate.leader
 * @description:
 * @Author: liyc
 * @CreateDate: 2018/8/10 上午12:18
 * @Version: 1.0
 * <p>Copyright: Copyright (c) 2018</p>
 */
public class Leader implements Itarget{

    private static Map<String,Itarget> itargetMap = new HashMap<>();



    public Leader() {
        itargetMap.put("TargetA",new TargetA());
        itargetMap.put("TargetB",new TargetB());
    }

    public void doing(String command){
        for (Map.Entry entry : itargetMap.entrySet()){
            if (command.equals(entry.getKey())){
                Itarget itarget = (Itarget) entry.getValue();
                itarget.doing(command);
            }
        }

    }
}
