package com.allsofts.pattern.course.template.dao;

import com.allsofts.pattern.course.template.JdbcTemplate;
import com.allsofts.pattern.course.template.RowMapper;
import com.allsofts.pattern.course.template.entity.Member;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.util.List;

/**
 * @ProjectName: pattern
 * @Package: com.allsofts.pattern.course.template.dao
 * @description:
 * @Author: liyc
 * @CreateDate: 2018/8/5 下午12:36
 * @Version: 1.0
 * <p>Copyright: Copyright (c) 2018</p>
 */
public class MemberDao {

    private JdbcTemplate jdbcTemplate = new JdbcTemplate(null);
    public MemberDao(DataSource dataSource) {
    }

    public List<?> query() throws Exception {
        String sql = "select * from t_member";
        return jdbcTemplate.executeQuery(sql, new RowMapper<Member>() {
            @Override
            public Object mapRow(ResultSet rs, int rowNum) throws Exception {
                Member member = new Member();
                member.setUsername(rs.getString("username"));
                member.setPassword(rs.getString("password"));
                member.setNickName(rs.getString("nickName"));
                member.setAge(rs.getInt("age"));
                member.setAddr(rs.getString("addr"));
                return member;
            }
        },null);

    }

}
