package com.allsofts.pattern.course.template.entity;

/**
 * @ProjectName: pattern
 * @Package: com.allsofts.pattern.course.template.entity
 * @description:
 * @Author: liyc
 * @CreateDate: 2018/8/5 下午12:30
 * @Version: 1.0
 * <p>Copyright: Copyright (c) 2018</p>
 */
public class Member {
    private String username;
    private String password;
    private String nickName;
    private int age;
    private String addr;

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getAddr() {
        return addr;
    }

    public void setAddr(String addr) {
        this.addr = addr;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }
}
