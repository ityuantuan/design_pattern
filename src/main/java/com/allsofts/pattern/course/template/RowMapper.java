package com.allsofts.pattern.course.template;

import java.sql.ResultSet;

/**
 * @ProjectName: pattern
 * @Package: com.allsofts.pattern.course.template
 * @description:
 * @Author: liyc
 * @CreateDate: 2018/8/5 下午1:20
 * @Version: 1.0
 * <p>Copyright: Copyright (c) 2018</p>
 */
public interface RowMapper<T> {
    public Object mapRow(ResultSet rs, int rowNum) throws Exception;

}
