package com.allsofts.pattern.course.factory;


import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Scanner;

public class CalculatorOne {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        while (true){
            System.out.println("请输入数字A:");
            String a = scanner.next();
            System.out.println("您输入的是:" + a);
            System.out.println("请输入运算符号:+-*/");
            String b = scanner.next();
            System.out.println("您输入的是:" + b);
            System.out.println("请输入数字B:");
            String c = scanner.next();
            System.out.println("您输入的是:" + c);
            String d = null;
            if (b.equals("+")) {
                d = new BigDecimal(a).add(new BigDecimal(c)).setScale(2, RoundingMode.HALF_UP).toString();

            } else if (b.equals("-")) {

                d = new BigDecimal(a).subtract(new BigDecimal(c)).setScale(2, RoundingMode.HALF_UP).toString();
            } else if (b.equals("*")) {

                d = new BigDecimal(a).multiply(new BigDecimal(c)).setScale(2, RoundingMode.HALF_UP).toString();
            } else if (b.equals("/")) {

                d = new BigDecimal(a).divide(new BigDecimal(c), 2, RoundingMode.HALF_UP).toString();
            }

            System.out.println("结果为:" + d);
        }
    }


}
