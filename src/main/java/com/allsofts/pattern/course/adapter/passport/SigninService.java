package com.allsofts.pattern.course.adapter.passport;

import com.allsofts.pattern.course.adapter.ResultMsg;
import com.allsofts.pattern.course.template.entity.Member;

/**
 * @ProjectName: pattern
 * @Package: com.allsofts.pattern.course.adapter.passport
 * @description:
 * @Author: liyc
 * @CreateDate: 2018/8/21 上午7:36
 * @Version: 1.0
 * <p>Copyright: Copyright (c) 2018</p>
 */
public class SigninService {


    /**
     * @param username
     * @param password
     * @return
     */
    public ResultMsg regist(String username, String password) {

        return new ResultMsg(200,"注册成功",new Member());
    }

    public ResultMsg login(String username, String password) {
        return new ResultMsg(200,"登录成功",new Member());
    }
}
