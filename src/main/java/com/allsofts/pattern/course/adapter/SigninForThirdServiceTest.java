package com.allsofts.pattern.course.adapter;

import com.allsofts.pattern.course.adapter.passport.SigninForThirdService;
import com.allsofts.pattern.course.adapter.passport.SigninService;

/**
 * @ProjectName: pattern
 * @Package: com.allsofts.pattern.course.adapter
 * @description:
 * @Author: liyc
 * @CreateDate: 2018/8/21 上午8:15
 * @Version: 1.0
 * <p>Copyright: Copyright (c) 2018</p>
 */
public class SigninForThirdServiceTest {

    public static void main(String[] args) {

        SigninForThirdService signinService = new SigninForThirdService();
        //还可以加一层策略模式
        //不改变原来的代码，也要满足现有需求
        signinService.loginForQQ("adfadfasdf");

    }
}
