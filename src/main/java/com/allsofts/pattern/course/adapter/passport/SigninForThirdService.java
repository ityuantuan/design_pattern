package com.allsofts.pattern.course.adapter.passport;

import com.allsofts.pattern.course.adapter.ResultMsg;

/**
 * @ProjectName: pattern
 * @Package: com.allsofts.pattern.course.adapter.passport
 * @description:
 * @Author: liyc
 * @CreateDate: 2018/8/21 上午8:02
 * @Version: 1.0
 * <p>Copyright: Copyright (c) 2018</p>
 */
public class SigninForThirdService extends SigninService {
    public ResultMsg loginForQQ(String openid){
        return this.loginForRegist(openid,null);

    }

    public ResultMsg loginForWeChat(String openId){
        return null;
    }
    public ResultMsg loginForToken(String token){
        return null;
    }
    public ResultMsg loginForTel(String tel){
        return null;
    }

    public ResultMsg loginForRegist(String userName,String password){
        //1.认为openid全局唯一，可以当做用户名
        //2.密码默认为null
        //3.注册（在原有的系统创建一个用户）
        ResultMsg resultMsg = super.regist(userName,password);
        //4.调用原来的登录方法
        resultMsg = super.login(userName,password);

        return resultMsg;
    }
}
