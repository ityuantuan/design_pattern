package com.allsofts.pattern.course.proxy.dynamic.jdk;

import com.allsofts.pattern.course.proxy.staticed.Person;
import sun.misc.ProxyGenerator;

import java.io.FileOutputStream;

public class JdkProxyTest {
    public static void main(String[] args) {
        FileOutputStream os = null;
        try {
//            XieMuInterface obj =  (XieMuInterface) new JdkMatchMaker().getInstance(new XieMu());
//            System.out.println(obj.getClass());
//            obj.findLove();
            byte [] bytes = ProxyGenerator.generateProxyClass("$Proxy0",new Class[]{XieMuInterface.class});
            os = new FileOutputStream("/Users/didi/$Proxy0.class");
            os.write(bytes);
            os.flush();
            os.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
