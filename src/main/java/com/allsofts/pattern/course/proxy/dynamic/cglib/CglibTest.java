package com.allsofts.pattern.course.proxy.dynamic.cglib;

public class CglibTest {
    public static void main(String[] args) throws Exception {
        CglibMeiPo cglibMeiPo = new CglibMeiPo();
        ZhangSan object = (ZhangSan) cglibMeiPo.getInstance(ZhangSan.class);
        object.findLove();
        System.out.println("---------------------");
        System.out.println(object.getClass());
    }
}
