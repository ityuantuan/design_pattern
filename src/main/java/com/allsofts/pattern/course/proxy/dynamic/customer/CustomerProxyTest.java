package com.allsofts.pattern.course.proxy.dynamic.customer;

import com.allsofts.pattern.course.proxy.dynamic.jdk.XieMu;
import com.allsofts.pattern.course.proxy.dynamic.jdk.XieMuInterface;

public class CustomerProxyTest {
    public static void main(String[] args) {
        try {
            XieMuInterface obj =  (XieMuInterface) new CustomerMeipo().getInstance(new XieMu());
            System.out.println(obj.getClass());
            obj.findLove();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
