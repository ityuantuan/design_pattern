package com.allsofts.pattern.course.proxy.dynamic.jdk;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

public class JdkMatchMaker implements InvocationHandler {

    //被代理的对象，把引用给保存下来
    private XieMuInterface target;

    public Object getInstance(XieMuInterface target) {
        this.target = target;
        Class<?> clazz = target.getClass();
        //用来生成一个新的对象（字节码重组）
        //下半节课，深入讲解字节码重组
        return Proxy.newProxyInstance(clazz.getClassLoader(), clazz.getInterfaces(), this);
    }

    /**
     * jdk:
     * 原理：
     * 1.拿到被代理对象的引用，并且获取到它的所有接口
     * 2.通过jdk Proxy 类重新生成一个新的类，同时新的类要实现被代理类所有实现
     * 3.把新加的业务逻辑方法由一定的逻辑代码去调用（在代码中实现）
     * 4.编译新生成的java代码.class
     * 5.再重新加载到jvm运行
     * 以上这个过程叫字节码重组
     * jdk中有个规范，只要是$开头的一般都是自动生成的
     * 通过反编译工具可以查看源代码
     **/
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.println("我是媒婆，我要给你找对象，现在拿到你的需求了");
        System.out.println("开始物色");
        Object obj = method.invoke(this.target, args);
        System.out.println("如果合适的话，准备办事");
        return obj;
    }
}
