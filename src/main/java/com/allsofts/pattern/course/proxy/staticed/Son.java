package com.allsofts.pattern.course.proxy.staticed;
/**
 * 静态代理son
 * **/
public class Son implements Person{
    public void findLove(){

        System.out.println("找对象，肤白貌美大长腿");
    }

//    @Override
//    public void rentHouse() {
//
//    }
//
//    @Override
//    public void buy() {
//
//    }
//
//    @Override
//    public void job() {
//
//    }
}
