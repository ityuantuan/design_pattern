package com.allsofts.pattern.course.prototype;

import java.util.ArrayList;

public class CloneTest {
    public static void main(String[] args) throws CloneNotSupportedException {
        CloneTarget p = new CloneTarget();
        p.name = "tom";
        p.list = new ArrayList<>();
        p.list.add(new CloneTarget());

        System.out.println(p.name);
        Prototype obj = (Prototype)p.clone();
        System.out.println(obj.name);
    }
}
