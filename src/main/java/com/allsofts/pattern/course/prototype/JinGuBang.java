package com.allsofts.pattern.course.prototype;

import java.io.Serializable;
/**
 * 齐天大圣的金箍棒
 * **/
public class JinGuBang implements Serializable {
    private float h = 100;
    private float d = 10;

    public void big() {
        this.d *= 2;
        this.h *= 2;
    }

    public void small() {
        this.d /= 2;
        this.h /= 2;
    }

}
