package com.allsofts.pattern.course.prototype;

import java.util.Date;
/**
 * 猴子父类
 * **/
public class Monkey {
    public int height;
    public int weight;
    public Date birthday;
}
