package com.allsofts.pattern.course.strategy.pay.payport;

import com.allsofts.pattern.course.strategy.pay.PayState;

/**
 * @ProjectName: pattern
 * @Package: com.allsofts.pattern.course.strategy.pay.payport
 * @description:
 * @Author: liyc
 * @CreateDate: 2018/8/4 下午6:19
 * @Version: 1.0
 * <p>Copyright: Copyright (c) 2018</p>
 */
public class AliPay implements Payment {
    public PayState pay(String uid, double amount) {
        System.out.println("欢迎使用支付宝");
        System.out.println("查询账户余额，开始扣款");
        return new PayState(200,"支付成功",amount);
    }
}
