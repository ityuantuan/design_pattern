package com.allsofts.pattern.course.strategy.pay;

/**
 * @ProjectName: pattern
 * @Package: com.allsofts.pattern.course.strategy.pay
 * @description:
 * @Author: liyc
 * @CreateDate: 2018/8/4 下午12:10
 * @Version: 1.0
 * <p>Copyright: Copyright (c) 2018</p>
 */
public class PayState
{
    private int code;
    private Object data;
    private String msg;

    public PayState(int code, String msg, Object data) {
        this.code = code;
        this.data = data;
        this.msg = msg;
    }

    @Override
    public String toString() {
        return "支付状态{" +
                "code=" + code +
                ", data=" + data +
                ", msg='" + msg + '\'' +
                '}';
    }

}
