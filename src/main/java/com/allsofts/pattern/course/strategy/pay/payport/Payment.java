package com.allsofts.pattern.course.strategy.pay.payport;

import com.allsofts.pattern.course.strategy.pay.PayState;

/**
 * @ProjectName: pattern
 * @Package: com.allsofts.pattern.course.strategy.pay.payport
 * @description:
 * @Author: liyc
 * @CreateDate: 2018/8/4 下午12:09
 * @Version: 1.0
 * <p>Copyright: Copyright (c) 2018</p>
 */
public interface Payment {
    public PayState pay(String uid,double amount);
}
