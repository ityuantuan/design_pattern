package com.allsofts.pattern.course.strategy.pay;

import com.allsofts.pattern.course.strategy.pay.payport.PayType;

/**
 * @ProjectName: pattern
 * @Package: com.allsofts.pattern.course.strategy.pay.payport
 * @description:
 * @Author: liyc
 * @CreateDate: 2018/8/4 上午11:54
 * @Version: 1.0
 * <p>Copyright: Copyright (c) 2018</p>
 */
public class Order {
    private String uid;
    private String orderId;
    private double amount;

    public Order(String uid, String orderId, double amount) {
        this.uid = uid;
        this.orderId = orderId;
        this.amount = amount;
    }


    public PayState pay(PayType payType) {
        //这个参数，完全可以用Payment这个接口来代替
        //为什么不用？
        //完美的解决了switch的过程，需要在代码逻辑中写switch了
        //更不需要if else
        return payType.get().pay(uid, amount);

    }


}
