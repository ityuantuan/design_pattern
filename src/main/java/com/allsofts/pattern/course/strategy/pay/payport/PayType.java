package com.allsofts.pattern.course.strategy.pay.payport;
/**
 * 姑且当做常量维护
 * **/


public enum PayType {

    ALI_PAY(new AliPay()),
    JDPAY(new JDPay()),
    WECHATPAY(new WeChatPay()),
    UNIONPAY(new UnionPay());

    private Payment payment;
    PayType(Payment payment){
        this.payment = payment;
    }
    public Payment get(){
        return this.payment;
    }

}
