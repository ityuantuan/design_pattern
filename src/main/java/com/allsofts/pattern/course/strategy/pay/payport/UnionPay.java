package com.allsofts.pattern.course.strategy.pay.payport;

import com.allsofts.pattern.course.strategy.pay.PayState;

/**
 * @ProjectName: pattern
 * @Package: com.allsofts.pattern.course.strategy.pay.payport
 * @description:
 * @Author: liyc
 * @CreateDate: 2018/8/4 下午6:28
 * @Version: 1.0
 * <p>Copyright: Copyright (c) 2018</p>
 */
public class UnionPay implements Payment {
    @Override
    public PayState pay(String uid, double amount) {
        System.out.println("欢迎使用银联卡");
        System.out.println("查询账户余额，开始扣款");
        return new PayState(200,"支付成功",amount);
    }
}
