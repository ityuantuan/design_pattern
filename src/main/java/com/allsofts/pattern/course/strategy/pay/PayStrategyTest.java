package com.allsofts.pattern.course.strategy.pay;

import com.allsofts.pattern.course.strategy.pay.payport.PayType;

import java.util.ArrayList;
import java.util.Comparator;

/**
 * @ProjectName: pattern
 * @Package: com.allsofts.pattern.course.strategy.pay.payport
 * @description:
 * @Author: liyc
 * @CreateDate: 2018/8/4 下午6:15
 * @Version: 1.0
 * <p>Copyright: Copyright (c) 2018</p>
 */
public class PayStrategyTest {

    public static void main(String[] args) {
        //省略把商品添加大购物车，在从购物车下单
        //直接从点单开始
        Order order = new Order("1","2018031100000100009",314.35);
        //开始支付，选择支付方式，微信支付，支付宝，银联卡，京东白条，财付通
        //每个渠道支付的具体算法不一样的
        //基本算法固定的

        //在支付的时候才决定
        System.out.println(order.pay(PayType.JDPAY));


        //使用策略模式
        new ArrayList<Object>().sort(new Comparator() {
            @Override
            public int compare(Object o1, Object o2) {
                return 0;
            }
        });

        //beanfactory
        //listablefactory
    }
}
