package com.allsofts.pattern.course.observer.subject;

import com.allsofts.pattern.course.observer.core.EventListener;

/**
 * @ProjectName: pattern
 * @Package: com.allsofts.pattern.course.observer.subject
 * @description:
 * @Author: liyc
 * @CreateDate: 2018/8/22 下午10:47
 * @Version: 1.0
 * <p>Copyright: Copyright (c) 2018</p>
 */
public class Subject extends EventListener {

    //通常的话，采用动态代理来实现监控，避免代码侵入
    public void add(){
        System.out.println("调用添加的方法");
        trigger(SubjectEventType.ON_ADD);

    }

    public void remove(){
        System.out.println("调用删除的方法");
        trigger(SubjectEventType.ON_REMOVE);

    }
}
