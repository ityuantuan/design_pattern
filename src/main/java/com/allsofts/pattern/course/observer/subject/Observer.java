package com.allsofts.pattern.course.observer.subject;

import com.allsofts.pattern.course.observer.core.Event;

/**
 * @ProjectName: pattern
 * @Package: com.allsofts.pattern.course.observer.subject
 * @description:
 * @Author: liyc
 * @CreateDate: 2018/8/22 下午10:48
 * @Version: 1.0
 * <p>Copyright: Copyright (c) 2018</p>
 */
public class Observer {

    public void advice(Event e) {
        System.out.println("====触发事件，打印日志====\n" + e);


    }
}
