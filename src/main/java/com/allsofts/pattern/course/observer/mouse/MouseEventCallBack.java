package com.allsofts.pattern.course.observer.mouse;

import com.allsofts.pattern.course.observer.core.Event;

/**
 * @ProjectName: pattern
 * @Package: com.allsofts.pattern.course.observer.mouse
 * @description:
 * @Author: liyc
 * @CreateDate: 2018/8/22 下午11:26
 * @Version: 1.0
 * <p>Copyright: Copyright (c) 2018</p>
 */
public class MouseEventCallBack {
    public void onClick(Event e) {
        System.out.println("=====触发鼠标onClick事件=====\n" + e);
    }
    public void onDoubleClick(Event e) {
        System.out.println("=====触发鼠标双击事件=====\n" + e);
    }
    public void onUp(Event e) {
        System.out.println("=====触发鼠标onUp事件=====\n" + e);
    }
    public void onDown(Event e) {
        System.out.println("=====触发鼠标onDown事件=====\n" + e);
    }
    public void onMove(Event e) {
        System.out.println("=====触发鼠标onMove事件=====\n" + e);
    }
    public void onOver(Event e) {
        System.out.println("=====触发鼠标onOver事件=====\n" + e);
    }
    public void onWheel(Event e) {
        System.out.println("=====触发鼠标onWheel事件=====\n" + e);
    }
}
