package com.allsofts.pattern.course.observer.mouse;

import com.allsofts.pattern.course.observer.core.Event;

import java.lang.reflect.Method;

/**
 * @ProjectName: pattern
 * @Package: com.allsofts.pattern.course.observer.mouse
 * @description:
 * @Author: liyc
 * @CreateDate: 2018/8/22 下午11:30
 * @Version: 1.0
 * <p>Copyright: Copyright (c) 2018</p>
 */
public class MouseTest {
    public static void main(String[] args) throws NoSuchMethodException {

        MouseEventCallBack callBack = new MouseEventCallBack();
        Method onClick = MouseEventCallBack.class.getMethod("onClick", Event.class);

        Mouse mouse = new Mouse();
        mouse.addListener(MouseEventType.ON_CLICK,callBack,onClick);
        mouse.click();

    }
}
