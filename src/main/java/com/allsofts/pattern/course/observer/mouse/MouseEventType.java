package com.allsofts.pattern.course.observer.mouse;

/**
 * @ProjectName: pattern
 * @Package: com.allsofts.pattern.course.observer.mouse
 * @description:
 * @Author: liyc
 * @CreateDate: 2018/8/22 下午11:23
 * @Version: 1.0
 * <p>Copyright: Copyright (c) 2018</p>
 */
public enum  MouseEventType {
    ON_CLICK,ON_DOUBLE_CLICK,ON_UP,ON_DOWN,ON_MOVE,ON_OVER,ON_WHEEL;
}
