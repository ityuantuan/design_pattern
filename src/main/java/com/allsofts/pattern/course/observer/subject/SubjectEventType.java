package com.allsofts.pattern.course.observer.subject;

public enum SubjectEventType {
    ON_ADD,ON_REMOVE,ON_UPDATE,ON_QUERY;
}
