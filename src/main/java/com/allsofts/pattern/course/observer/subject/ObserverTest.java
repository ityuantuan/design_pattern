package com.allsofts.pattern.course.observer.subject;

import com.allsofts.pattern.course.observer.core.Event;

import java.lang.reflect.Method;

/**
 * @ProjectName: pattern
 * @Package: com.allsofts.pattern.course.observer.subject
 * @description:
 * @Author: liyc
 * @CreateDate: 2018/8/22 下午11:09
 * @Version: 1.0
 * <p>Copyright: Copyright (c) 2018</p>
 */
public class ObserverTest {

    public static void main(String[] args) throws NoSuchMethodException {

        //观察者
        Observer observer = new Observer();
        Observer observer1 = new Observer();
        Method advice = Observer.class.getMethod("advice", new Class<?>[]{Event.class});

        Subject subject = new Subject();
        subject.addListener(SubjectEventType.ON_ADD,observer,advice);
        subject.addListener(SubjectEventType.ON_ADD,observer1,advice);
        subject.add();
    }
}
