package com.allsofts.pattern.course.observer.mouse;

import com.allsofts.pattern.course.observer.core.EventListener;

/**
 * @ProjectName: pattern
 * @Package: com.allsofts.pattern.course.observer.mouse
 * @description:
 * @Author: liyc
 * @CreateDate: 2018/8/22 下午11:24
 * @Version: 1.0
 * <p>Copyright: Copyright (c) 2018</p>
 */
public class Mouse extends EventListener {
    public void click(){
        System.out.println("鼠标click");
        this.trigger(MouseEventType.ON_CLICK);
    }
    public void doubleClick(){
        System.out.println("鼠标doubleClick");

        this.trigger(MouseEventType.ON_DOUBLE_CLICK);
    }
    public void up(){
        System.out.println("鼠标up");

        this.trigger(MouseEventType.ON_UP);
    }
    public void down(){
        System.out.println("鼠标down");

        this.trigger(MouseEventType.ON_DOWN);
    }
    public void wheel(){
        System.out.println("鼠标wheel");
        this.trigger(MouseEventType.ON_WHEEL);
    }
    public void move(){
        System.out.println("鼠标move");

        this.trigger(MouseEventType.ON_MOVE);
    }
    public void over(){
        System.out.println("鼠标over");

        this.trigger(MouseEventType.ON_OVER);
    }
}
