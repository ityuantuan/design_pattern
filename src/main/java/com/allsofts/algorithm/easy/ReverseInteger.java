package com.allsofts.algorithm.easy;

import java.math.BigInteger;

/**
 * @ProjectName: pattern
 * @Package: com.allsofts.algorithm.easy
 * @description:
 * @Author: liyc
 * @CreateDate: 2019/1/10 5:40 PM
 * @Version: 1.0
 * <p>Copyright: Copyright (c) 2019</p>
 */
public class ReverseInteger {
    public int reverse(int param) {

        long x = param;
        boolean symbol = false;
        if (x < 0) {
            x = Math.abs(x);
            symbol = true;
        }
        char[] str = String.valueOf(x).toCharArray();
        char[] result = new char[str.length];
        for (int i = 0; i < str.length; i++) {
            result[i] = str[str.length - i - 1];
        }
        String retStr = new String(result);

        if (symbol) {
            retStr = "-" + retStr;
        }
        Long l = Long.valueOf(retStr);
        System.out.println(l);
        if (l > Integer.MAX_VALUE || l < Integer.MIN_VALUE) {
            return 0;
        } else {
            return l.intValue();
        }
    }

    public static void main(String[] args) {
        System.out.println(new ReverseInteger().reverse(-2147483648));
//        System.out.println(Integer.valueOf("9646324351"));
//        System.out.println(0xffffffff);
    }
}
