package com.allsofts.algorithm.easy;

import java.util.HashMap;
import java.util.Map;

/**
 * @ProjectName: pattern
 * @Package: com.allsofts.algorithm.easy
 * @description:
 * @Author: liyc
 * @CreateDate: 2019/1/10 12:07 PM
 * @Version: 1.0
 * <p>Copyright: Copyright (c) 2019</p>
 */
public class TwoSum {


    public static int[] gfTwoSum(int[] nums, int target) {
        Map<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            map.put(i,target - nums[i]);
        }

        int[] temp = new int[nums.length];
        int index = 0;
        for (int i = 0; i < nums.length; i++) {
            if (map.containsKey(nums[i])&&map.get(nums[i])!=i) {
                temp[index] = map.get(nums[i]);
                index++;
                map.remove(nums[i]);
            }
        }
        int result[] = new int[index];
        for (int i = 0; i < index; i++) {
            result[i] = temp[i];
        }
        return result;
    }

    public static int[] twoSum(int[] nums, int target) {
        int ys[] = new int[nums.length];
        int result[] = new int[nums.length];
        for (int i = 0; i < nums.length; i++) {
            int rest = target - nums[i];
            ys[i] = rest;
            result[i] = -1;
        }
        int index = 0;
        for (int i = 0; i < nums.length; i++) {
            for (int j = 0; j < nums.length; j++) {
                if (ys[j] == nums[i] && i != j) {
                    result[index] = i;
                    index++;
                }
            }
        }
        int[] returnNum = new int[index];
        for (int i = 0; i < index; i++) {
            returnNum[i] = result[i];
        }
        return returnNum;
    }


    public static void main(String[] args) {
        int[] nums = new int[]{3,3};
        int target = 6;
        int[] result = gfTwoSum(nums, target);
        for (int i : result) {
            System.out.println(i);

        }


    }

}
