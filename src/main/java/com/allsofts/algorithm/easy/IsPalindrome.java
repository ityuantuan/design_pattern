package com.allsofts.algorithm.easy;

/**
 * @ProjectName: pattern
 * @Package: com.allsofts.algorithm.easy
 * @description:
 * @Author: liyc
 * @CreateDate: 2019/1/11 4:03 PM
 * @Version: 1.0
 * <p>Copyright: Copyright (c) 2019</p>
 */
public class IsPalindrome {
    public boolean isPalindromeStr(int x) {
        long y = x;
        String oldStr = String.valueOf(y);

        if (y < 0) {
            return false;
        }
        char[] str = String.valueOf(y).toCharArray();
        char[] result = new char[str.length];
        for (int i = 0; i < str.length; i++) {
            result[i] = str[str.length - i - 1];
        }
        String newStr = new String(result);
        if (newStr.equals(oldStr)) {
            return true;
        }
        return false;
    }

    public boolean isPalindromeInt(int x) {

        String reverseNumber = new StringBuilder(String.valueOf(x)).reverse().toString();
        return reverseNumber.equals(String.valueOf(x));
    }

    public static void main(String[] args) {

    }
}
